// Copyright 2008,2009,2010 Massachusetts Institute of Technology.
// All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

#include "crc32c/crc32c.h"

#include <cassert>
#include <cstdio>
#include <cstring>

namespace logging {

#include "crc32c/crc32c.c"

}  // namespace logging
